import axios from "axios";

export const BASE_URL = 'http://172.20.10.9:8001'

export const getData = async (url, params) => {
    try {
        const response = await axios.get(`${BASE_URL}${url}`, {
            params
        });
        const { data } = response;

        return data;
    } catch (error) {
        const errorMessage = error.response && error.response.data && error.response.data.errors ? error.response.data?.errors : '';

        return { errors: errorMessage };
    }
};
export const postData = async (url, params, method = 'POST') => {
    try {
        const response = await axios({
            method,
            url: `${BASE_URL}${url}`,
            data: params
        });
        const { data } = response;
        return data;
    } catch (error) {
        const errorMessage = error.response && error.response.data && error.response.data.errors ? error.response.data.errors : '';

        return { errors: errorMessage, error: error?.response?.data?.error };
    }
};
