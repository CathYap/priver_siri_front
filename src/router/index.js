import { createRouter, createWebHistory } from 'vue-router'
import MainComponent from "../views/MainComponent.vue";
import Map from "@/components/Map.vue";

const routes = [
  {
    path: '/',
    name: 'home',
    component: MainComponent
  },
  {
    path: '/map',
    name: 'map',
    component: Map
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
