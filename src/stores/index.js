import {defineStore} from "pinia";
import {getData} from "@/api/axios";

export const useMainStore = defineStore('mainStore', {
    state: () => ({ museums: [], markers: [ ], data: null }),
    getters: {
        doubleName: (state) => state.name * 2,
    },
    actions: {
        async getMuseums() {
            try {
                this.museums = await getData('/author/museum')
            } catch (error) {
                return error
            }
        },
        async getMarkers({id}) {
            try {
                this.markers = await getData(`/history-message/${id}`)

            } catch (error) {
                return error
            }
        },
    },
})
