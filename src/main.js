import { createApp } from 'vue'
import App from './App.vue'
import { createPinia } from 'pinia'

import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import '@mdi/font/css/materialdesignicons.css'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import YmapPlugin from 'vue-yandex-maps'

import router from './router'

const vuetify = createVuetify({
    components,
    directives,
})

const pinia = createPinia()

const settings = {
    apiKey: 'ba0c5d25-22fa-4bbb-8108-9b0b01320924',
    lang: 'ru_RU', // Используемый язык
    coordorder: 'latlong', // Порядок задания географических координат
    debug: false, // Режим отладки
    version: '2.1' // Версия Я.Карт
}


const app = createApp(App)

app.use(router)
app.use(pinia)
app.use(vuetify)
app.use(YmapPlugin, settings)
app.mount('#app')
